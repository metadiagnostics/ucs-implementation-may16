/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.socraticgrid.hl7.ucs.nifi.services;

import java.util.HashMap;
import java.util.Map;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.util.MockConfigurationContext;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import org.junit.After;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.spy;
import org.mockserver.integration.ClientAndServer;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import org.mockserver.model.Header;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.socraticgrid.hl7.services.uc.model.UserContactInfo;
import org.socraticgrid.hl7.ucs.nifi.controller.user.HTTPContactInfoResolver;

/**
 *
 * @author esteban
 */
public class HTTPContactInfoResolverTest {

    private HTTPContactInfoResolver contactInfoResolverService;
    private ClientAndServer mockServer;

    @Before
    public void doBefore() throws Exception {

        this.mockServer = startClientAndServer(9999);
        this.configureServerResource(mockServer, "/userInfo/resolve/123", "GET", "ealiverti, ealiverti@cognitivemedicine.com, 09876543212, ealiverti@socraticgrid.org, +09876543212");

        Map<PropertyDescriptor, String> properties = new HashMap<>();
        properties.put(HTTPContactInfoResolver.CIR_URL, "http://localhost:9999/userInfo/resolve");

        ConfigurationContext context = new MockConfigurationContext(properties, null);

        contactInfoResolverService = spy(new HTTPContactInfoResolver());
        contactInfoResolverService.onEnabled(context);

    }
    
    @After
    public void doAfetr(){
        if (this.mockServer != null){
            this.mockServer.stop(true);
        }
    }

    @Test
    public void testExistingUser() {
        UserContactInfo uci = this.contactInfoResolverService.resolveUserContactInfo("123");
        
        assertThat(uci, not(nullValue()));
        assertThat(uci.getName(), is("ealiverti"));
        assertThat(uci.getAddressesByType().get(HTTPContactInfoResolver.SERVICE_TYPE_EMAIL).getAddress(), is("ealiverti@cognitivemedicine.com"));
        assertThat(uci.getAddressesByType().get(HTTPContactInfoResolver.SERVICE_TYPE_SMS).getAddress(), is("09876543212"));
        assertThat(uci.getAddressesByType().get(HTTPContactInfoResolver.SERVICE_TYPE_CHAT).getAddress(), is("ealiverti@socraticgrid.org"));
        assertThat(uci.getAddressesByType().get(HTTPContactInfoResolver.SERVICE_TYPE_TEXT_TO_VOICE).getAddress(), is("+09876543212"));
        
    }

    private void configureServerResource(ClientAndServer server, String url, String operation, String body) {
        server.when(
            HttpRequest.request()
            .withMethod(operation.toUpperCase())
            .withPath(url)
        )
            .respond(
                HttpResponse.response()
                .withStatusCode(200)
                .withHeader(new Header("Content-Type", "text/plain; charset=utf-8"))
                .withBody(body));
    }
}
