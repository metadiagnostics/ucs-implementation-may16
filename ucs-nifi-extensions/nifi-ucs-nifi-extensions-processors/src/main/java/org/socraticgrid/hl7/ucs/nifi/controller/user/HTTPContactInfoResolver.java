/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.socraticgrid.hl7.ucs.nifi.controller.user;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.nifi.annotation.lifecycle.OnEnabled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.controller.AbstractControllerService;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.processor.util.StandardValidators;
import org.socraticgrid.hl7.services.uc.model.PhysicalAddress;
import org.socraticgrid.hl7.services.uc.model.UserContactInfo;

/**
 * {@link UserContactInfoResolverController} implementation that uses an HTTP
 * GET operation to resolve the contact information of a user.
 * 
 * This class supports 2 configuration options:
 * <ul>
 * <li>url: the URL being used for the GET operation. A new element will be
 * appended by this controller containing the id of the user being resolved.</li>
 * <li>cache-resuts: a boolean property indicating whether the results from
 * the GET operations should be cached or not to avoid unnecessary network
 * traffic</li>
 * </ul>
 * 
 * The response of the HTTP Get operation expected by this class is a text/plain
 * response with Code 200. The content of the response must be a single line
 * containing the user information formatted using a comma separated value format.
 * The expected format is: 
 * <code>name, email, telephone number, chat id, text to voice number</code>
 * 
 * @author esteban
 */
public class HTTPContactInfoResolver extends AbstractControllerService implements UserContactInfoResolverController {
    
    public static final String SERVICE_TYPE_SMS = "SMS";
    public static final String SERVICE_TYPE_EMAIL = "EMAIL";
    public static final String SERVICE_TYPE_CHAT = "CHAT";
    public static final String SERVICE_TYPE_TEXT_TO_VOICE = "TEXT-TO-VOICE";

    private String cirURL;
    private boolean cacheResults;

    private Cache<String, UserContactInfo> cache = new Cache<>(100);

    public static final PropertyDescriptor CIR_URL = new PropertyDescriptor.Builder()
        .name("url")
        .description("Contact Info Resolver URL")
        .required(true)
        .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
        .addValidator(StandardValidators.URL_VALIDATOR)
        .build();

    public static final PropertyDescriptor CIR_CACHE_INFO = new PropertyDescriptor.Builder()
        .name("cache-resuts")
        .description("Tells this controller whether the Contact Information previously resolved should be cached or not. The default value is false.")
        .required(true)
        .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
        .addValidator(StandardValidators.BOOLEAN_VALIDATOR)
        .defaultValue("false")
        .build();

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        final List<PropertyDescriptor> descriptors = new ArrayList<>();
        descriptors.add(CIR_URL);
        descriptors.add(CIR_CACHE_INFO);
        return descriptors;
    }

    @OnEnabled
    public void onEnabled(final ConfigurationContext context) throws Exception {
        this.cirURL = context.getProperty(CIR_URL).getValue();
        this.cacheResults = context.getProperty(CIR_CACHE_INFO).asBoolean();
    }

    @Override
    public UserContactInfo resolveUserContactInfo(String userId) {

        //If cache is enabled and the user is already there, return the data.
        if (this.cacheResults && this.cache.get(userId) != null) {
            return this.cache.get(userId);
        }

        //resolve the user information
        UserContactInfo info = this.getUserContactInfoFromURL(this.cirURL, userId);

        //if cache is enabled, cache the information
        if (this.cacheResults) {
            this.cache.put(userId, info);
        }

        return info;
    }

    private UserContactInfo getUserContactInfoFromURL(String cirURL, String userId) {

        try {
            HttpClient client = HttpClientBuilder.create().build();

            cirURL += cirURL.endsWith("/")?userId:"/"+userId;
            
            URI uri = new URIBuilder(cirURL)
                .build();

            HttpGet get = new HttpGet(uri);
            
            HttpResponse response = client.execute(get);
            
            if (response.getStatusLine().getStatusCode() != 200){
                throw new IllegalArgumentException("Unexpected response code from "+uri+". Expected 200, received "+response.getStatusLine().getStatusCode());
            }
            
            String responseText = IOUtils.toString(response.getEntity().getContent());
            
            String[] parts = responseText.trim().split(",");
                if (parts.length != 5){
                    throw new IllegalArgumentException("The response from "+uri+ " doesn't contain 5 elements. The response was: "+responseText);
                }
                
                return this.mockUserContactInfo(parts[0].trim(), parts[1].trim(), parts[2].trim(), parts[3].trim(), parts[4].trim());
            
        } catch (IOException | URISyntaxException ex) {
            throw new IllegalStateException("Exception getting user contact information from "+cirURL, ex);
        }
    }
    

    private UserContactInfo mockUserContactInfo(String name, String email, String phoneNumber, String chatHandle, String textToVoiceNumber){
        Map<String, PhysicalAddress> addressesByType = new HashMap<>();
        
        addressesByType.put(HTTPContactInfoResolver.SERVICE_TYPE_EMAIL, new PhysicalAddress(MOCKUserContactInfoResolverControllerImpl.SERVICE_TYPE_EMAIL, email));
        addressesByType.put(HTTPContactInfoResolver.SERVICE_TYPE_SMS, new PhysicalAddress(MOCKUserContactInfoResolverControllerImpl.SERVICE_TYPE_SMS, phoneNumber));
        addressesByType.put(HTTPContactInfoResolver.SERVICE_TYPE_CHAT, new PhysicalAddress(MOCKUserContactInfoResolverControllerImpl.SERVICE_TYPE_CHAT, chatHandle));
        addressesByType.put(HTTPContactInfoResolver.SERVICE_TYPE_TEXT_TO_VOICE, new PhysicalAddress(MOCKUserContactInfoResolverControllerImpl.SERVICE_TYPE_TEXT_TO_VOICE, textToVoiceNumber));
        
        UserContactInfo uci = new UserContactInfo();
        uci.setName(name);
        uci.setAddressesByType(addressesByType);
        
        uci.setPreferredAddress(addressesByType.values().iterator().next());
        
        return uci;
    }
    
}

class Cache<K, T> {

    private final Map<K, T> cache = new LinkedHashMap<>();
    private final int cacheSize;

    public Cache(int size) {
        this.cacheSize = size;
    }

    public synchronized void put(K key, T value) {
        if (this.cache.size() >= this.cacheSize && this.cacheSize > 0) {
            K oldestKey = this.cache.keySet().iterator().next();
            remove(oldestKey);
        }
        this.cache.put(key, value);
    }

    public synchronized T get(K key) {
        return this.cache.get(key);
    }

    public synchronized void clear() {
        this.cache.clear();
    }

    public synchronized T remove(K key) {
        return this.cache.remove(key);
    }

}
